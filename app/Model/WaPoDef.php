<?php
	App::uses('AppModel', 'Model');

	class WaPoDef extends AppModel {

		public $useTable = 'wa_po_defs';

		public $displayField = 'title';


		//The Associations below have been created with all possible keys, those that are not needed can be removed

		public $hasMany = array(
			'WaPo' => array(
				'className' => 'WaPo',
				'foreignKey' => 'wa_po_def_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			)
		);

		public function afterFind($results, $primary = false) {
			if ($results) {
				$this->_toFloat($results, ['limit']);
			}
			return $results;
		}

	}

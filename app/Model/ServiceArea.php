<?php
	App::uses('AppModel', 'Model');

	class ServiceArea extends AppModel {

		public $displayField = 'ZIP';

		public $actsAs = ['Containable'];

		public $findMethods = ['zip' =>  true];

		//The Associations below have been created with all possible keys, those that are not needed can be removed

		public $belongsTo = array(
			'Well' => array(
				'className' => 'Well',
				'foreignKey' => 'well_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

		public function findAllZips() {
			$sa = $this->find('all', [
				'fields' => 'ZIP',
				'group' => 'ZIP'
			]);
			return Hash::extract($sa, '{n}.ServiceArea.ZIP');
		}

		protected function _findZip($state, $query, $results = []) {
			if ($state === 'before') {
				if (isset($query['zip'])) {
					$query = array_merge($query, [
						'contain' => ['Well' => ['WaMe' => ['DataOrigin', 'WaPo' => ['WaPoDef']]]],
						'conditions' => ['ServiceArea.zip' => $query['zip']],
						'limit' => 1
					]);
					unset($query['zip']);
				}
				return $query;
			}

			if ($state !== 'after' || empty($results)) {
				return $results;
			}

			$WaPos = [];
			$WaMe = $results[0]['Well']['WaMe'][0];
			$WaPosRaw = $WaMe['WaPo'];
			foreach ($WaPosRaw as $WaPo) {
				$title = $WaPo['WaPoDef']['title'];
				$WaPos[] = [
					'title' => __d('m', $title),
					'type' => $title,
					'unit' => $WaPo['WaPoDef']['unit'],
					'description' => $WaPo['WaPoDef']['description'],
					'description-url' => $WaPo['WaPoDef']['url'],
					'limit' => $WaPo['WaPoDef']['limit'],
					'value' => $WaPo['value']
				];
			}
			$results = $results[0];
			$results['Well']['WaPos'] = Hash::sort($WaPos, '{n}.title', 'asc');
			$results['Well']['WaMe'] = $WaMe;
			return $results;
		}
	}

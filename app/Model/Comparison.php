<?php

	App::uses('AppModel', 'Model');

	class Comparison extends AppModel {

		public $belongsTo = array(
			'DataOrigin' => array(
				'className' => 'DataOrigin',
				'foreignKey' => 'data_origin_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

		protected $_hue = 0;

		public $actsAs = ['Containable'];

		public function get() {
			$comp = $this->find('all', [
				'contain' => ['DataOrigin'],
				'order' => ['label' => 'asc']
			]);
			foreach ($comp as &$c) {
				$c['Comparison']['color'] = "hsl({$this->_getHue()}, 90%, 40%)";
			}
			return Hash::extract($comp, '{n}.Comparison');
		}

		public function afterFind($results, $primary = false) {
			$alias = $this->alias;
			foreach ($results as $rK => $result) {
				foreach ($result[$alias] as $vK => $value) {
					if (is_numeric($value)) {
						$results[$rK][$alias][$vK] = (float)$value;
					}
				}
			}
			return $results;
		}

		protected function _getHue() {
			$this->_hue = ($this->_hue + 25) % 360;
			return $this->_hue;
		}

	}

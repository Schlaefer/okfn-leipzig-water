<?php
	App::uses('AppModel', 'Model');

	class WaPo extends AppModel {

		//The Associations below have been created with all possible keys, those that are not needed can be removed

		public $belongsTo = array(
			'WaMe' => array(
				'className' => 'WaMe',
				'foreignKey' => 'wa_me_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			),
			'WaPoDef' => array(
				'className' => 'WaPoDef',
				'foreignKey' => 'wa_po_def_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

		public function afterFind($results, $primary = false) {
			if ($results) {
				$this->_toFloat($results, ['value']);
			}
			return $results;
		}

	}

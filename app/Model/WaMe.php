<?php
	App::uses('AppModel', 'Model');

	class WaMe extends AppModel {


		//The Associations below have been created with all possible keys, those that are not needed can be removed

		public $belongsTo = array(
			'Well' => array(
				'className' => 'Well',
				'foreignKey' => 'well_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			),
			'DataOrigin' => array(
				'className' => 'DataOrigin',
				'foreignKey' => 'data_origin_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

		public $hasMany = array(
			'WaPo' => array(
				'className' => 'WaPo',
				'foreignKey' => 'wa_me_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			)
		);

	}

<?php
	App::uses('AppModel', 'Model');

	class Well extends AppModel {

		public $actsAs = ['Containable'];

		/**
		 * Display field
		 *
		 * @var string
		 */
		public $displayField = 'title';


		//The Associations below have been created with all possible keys, those that are not needed can be removed

		/**
		 * hasMany associations
		 *
		 * @var array
		 */
		public $hasMany = array(
			'ServiceArea' => array(
				'className' => 'ServiceArea',
				'foreignKey' => 'well_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			),
			'WaMe' => array(
				'className' => 'WaMe',
				'foreignKey' => 'well_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => 'WaMe.date DESC',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			)
		);

	}

<?php

	App::uses('Model', 'Model');

	class AppModel extends Model {

		protected function _toFloat(&$data, $keys) {
			$multiple = isset($data[0][$this->alias]);
			if ($multiple) {
				foreach($data as &$datum) {
					$this->_toFloat($datum[$this->alias], $keys);
				}
			}
			foreach($keys as $key) {
				if (isset($data[$key])) {
					$data[$key] = (float)$data[$key];
				}
			}
		}
	}

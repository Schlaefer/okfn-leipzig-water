<?php
/**
 * WaPoDefFixture
 *
 */
class WaPoDefFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'wa_po_defs';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'unit' => array('type' => 'string', 'null' => false, 'length' => 16, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'limit' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'recommended' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'unit' => 'Lorem ipsum do',
			'limit' => 1,
			'recommended' => 1,
			'created' => '2014-05-17 14:07:40',
			'modified' => '2014-05-17 14:07:40'
		),
	);

}

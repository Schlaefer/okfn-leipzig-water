<?php
/**
 * WaPoFixture
 *
 */
class WaPoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'wa_me_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'wa_po_def_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'value' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'wa_me_id' => 1,
			'wa_po_def_id' => 1,
			'value' => 1,
			'created' => '2014-05-17 14:07:27',
			'modified' => '2014-05-17 14:07:27'
		),
	);

}

<?php
/**
 * ServiceAreaFixture
 *
 */
class ServiceAreaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'well_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'ZIP' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 9, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'ZIP' => array('column' => 'ZIP', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'well_id' => 1,
			'ZIP' => 'Lorem i',
			'created' => '2014-05-07 08:08:27',
			'modified' => '2014-05-07 08:08:27'
		),
	);

}

<?php
App::uses('Well', 'Model');

/**
 * Well Test Case
 *
 */
class WellTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.well',
		'app.service_area',
		'app.wa_me'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Well = ClassRegistry::init('Well');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Well);

		parent::tearDown();
	}

}

<?php
App::uses('ServiceArea', 'Model');

/**
 * ServiceArea Test Case
 *
 */
class ServiceAreaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.service_area',
		'app.well'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ServiceArea = ClassRegistry::init('ServiceArea');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ServiceArea);

		parent::tearDown();
	}

}

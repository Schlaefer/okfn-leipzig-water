<?php
App::uses('WaMe', 'Model');

/**
 * WaMe Test Case
 *
 */
class WaMeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wa_me',
		'app.well',
		'app.service_area',
		'app.wa_po'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WaMe = ClassRegistry::init('WaMe');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WaMe);

		parent::tearDown();
	}

}

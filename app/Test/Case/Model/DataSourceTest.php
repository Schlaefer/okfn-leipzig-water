<?php
App::uses('DataSource', 'Model');

/**
 * DataSource Test Case
 *
 */
class DataSourceTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DataSource = ClassRegistry::init('DataSource');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DataSource);

		parent::tearDown();
	}

/**
 * testListSources method
 *
 * @return void
 */
	public function testListSources() {
		$this->markTestIncomplete('testListSources not implemented.');
	}

/**
 * testDescribe method
 *
 * @return void
 */
	public function testDescribe() {
		$this->markTestIncomplete('testDescribe not implemented.');
	}

/**
 * testBegin method
 *
 * @return void
 */
	public function testBegin() {
		$this->markTestIncomplete('testBegin not implemented.');
	}

/**
 * testCommit method
 *
 * @return void
 */
	public function testCommit() {
		$this->markTestIncomplete('testCommit not implemented.');
	}

/**
 * testRollback method
 *
 * @return void
 */
	public function testRollback() {
		$this->markTestIncomplete('testRollback not implemented.');
	}

/**
 * testColumn method
 *
 * @return void
 */
	public function testColumn() {
		$this->markTestIncomplete('testColumn not implemented.');
	}

/**
 * testCreate method
 *
 * @return void
 */
	public function testCreate() {
		$this->markTestIncomplete('testCreate not implemented.');
	}

/**
 * testRead method
 *
 * @return void
 */
	public function testRead() {
		$this->markTestIncomplete('testRead not implemented.');
	}

/**
 * testUpdate method
 *
 * @return void
 */
	public function testUpdate() {
		$this->markTestIncomplete('testUpdate not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testLastInsertId method
 *
 * @return void
 */
	public function testLastInsertId() {
		$this->markTestIncomplete('testLastInsertId not implemented.');
	}

/**
 * testLastNumRows method
 *
 * @return void
 */
	public function testLastNumRows() {
		$this->markTestIncomplete('testLastNumRows not implemented.');
	}

/**
 * testLastAffected method
 *
 * @return void
 */
	public function testLastAffected() {
		$this->markTestIncomplete('testLastAffected not implemented.');
	}

/**
 * testEnabled method
 *
 * @return void
 */
	public function testEnabled() {
		$this->markTestIncomplete('testEnabled not implemented.');
	}

/**
 * testSetConfig method
 *
 * @return void
 */
	public function testSetConfig() {
		$this->markTestIncomplete('testSetConfig not implemented.');
	}

/**
 * testInsertQueryData method
 *
 * @return void
 */
	public function testInsertQueryData() {
		$this->markTestIncomplete('testInsertQueryData not implemented.');
	}

/**
 * testResolveKey method
 *
 * @return void
 */
	public function testResolveKey() {
		$this->markTestIncomplete('testResolveKey not implemented.');
	}

/**
 * testGetSchemaName method
 *
 * @return void
 */
	public function testGetSchemaName() {
		$this->markTestIncomplete('testGetSchemaName not implemented.');
	}

/**
 * testClose method
 *
 * @return void
 */
	public function testClose() {
		$this->markTestIncomplete('testClose not implemented.');
	}

}

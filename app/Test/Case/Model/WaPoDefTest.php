<?php
App::uses('WaPoDef', 'Model');

/**
 * WaPoDef Test Case
 *
 */
class WaPoDefTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wa_po_def',
		'app.wa_po',
		'app.wa_me',
		'app.well',
		'app.service_area'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WaPoDef = ClassRegistry::init('WaPoDef');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WaPoDef);

		parent::tearDown();
	}

}

<?php
App::uses('WaPo', 'Model');

/**
 * WaPo Test Case
 *
 */
class WaPoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.wa_po',
		'app.wa_me',
		'app.well',
		'app.service_area',
		'app.wa_po_def'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WaPo = ClassRegistry::init('WaPo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WaPo);

		parent::tearDown();
	}

}

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
	<?= $this->Html->charset(); ?>
	<title>
		<?= $this->fetch('title'); ?>
	</title>
	<link rel="shortcut icon" href="<?= $this->webroot ?>favicon.ico?v=2" />
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Dosis:400,500'
				rel='stylesheet' type='text/css'>
	<script>
		window.app = { webroot: '<?= $this->webroot ?>' };
	</script>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');

		if (Configure::read('debug')) {
			echo $this->Html->css([
				'../bower_components/bootstrap/dist/css/bootstrap.min',
				'../bower_components/fontawesome/css/font-awesome.css',
			]);
			echo $this->Html->css('styles.less',
				['rel' => 'stylesheet/less', 'ext' => false]);
			echo $this->Html->script('../bower_components/less/dist/less-1.7.0.min.js');
		} else {
			echo $this->Html->css('../dist/styles.min');
		}

		echo $this->Layout->rjs();
	?>
</head>
<body>
<header id="header">
	<h1 id="title" class="container">
		<?= $this->Html->link('Trinkwasser in Leipzig', '/') ?>
	</h1>
</header>
<div id="topMenu">
	<div class="topMenu-form container">
		<?= $this->fetch('topMenu') ?>
	</div>
</div>
<div id="content" class="container">
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
</div>
<div id="footer">
	Proudly made in Leipzig; Germany. – <?php
		// echo $this->Html->link(__('Impressum'), '/impressum')
	?>
</div>
<?php
	echo $this->fetch('script');
	echo $this->element('sql_dump');
?>
</body>
</html>

<?php
	$this->start('topMenu');
	echo $this->Form->create(null, ['id' => 'form-zip', 'class' => 'isValid', 'type' => 'GET']);
	echo $this->Form->input('zip',
		['type' => 'textfield', 'name' => 'zip', 'label' => __('ZIP'), 'value' => $zip,
			'maxlength' => '5', 'div' => false, 'id' => 'zip-textfield'
		]);
	echo $this->Form->button(__('Send'), ['div' => false]);
	echo $this->Form->end();
	$this->end('topMenu');
?>
<script>
	window.app.data = {
		comp: <?= json_encode($comp) ?>,
		wapos: <?= json_encode($gauges) ?>,
		zips: <?= json_encode($zips) ?>
	};
</script>
<div class="sticky">
	<div id="sidebar">
		<?= $this->Form->button(__('Toggle Details'), ['id' => 'ha']) ?>
		<div id="compare"></div>
	</div>
</div>
<div class="wame-origin row">
	<div class="col-sm-push-1 col-sm-7">
		<p>
			<?= $well['title'] . ' – ' . h($origin['description']) ?>
		</p>
		</div>
</div>
<?php
	foreach ($gauges as $gauge) {
		echo $this->element('wapos/gauge', compact('gauge'));
	}

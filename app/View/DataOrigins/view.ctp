<div class="staticContent">
	<?php
		$this->start('topMenu');
		echo $this->Html->tag('h2', __('source.a'));
		$this->end('topMenu');


		$description = nl2br($origin['DataOrigin']['description']);
		echo $this->Html->para(null, $description);

		$url = $origin['DataOrigin']['url'];
		if (!empty($url)) {
			echo $this->Html->para(null, $this->Html->link($url, $url));
		}
	?>
</div>

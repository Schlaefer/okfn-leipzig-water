<?php

	App::uses('AppHelper', 'View/Helper');

	class LayoutHelper extends AppHelper {

		public $helpers = ['Html'];

		public function rjs() {
			if (Configure::read('debug')) {
				$main = 'js/main';
				$rjs = 'bower_components/requirejs/require';
			} else {
				$main = 'dist/main.min';
				$rjs = 'dist/require';
			}
			$main = $this->Html->assetUrl($main,
				['ext' => '.js', 'fullBase' => true]);
			$rjs = $this->Html->assetUrl($rjs, ['ext' => '.js', 'fullBase' => true]);
			return "<script data-main='$main' src='$rjs'></script>";
		}
	}

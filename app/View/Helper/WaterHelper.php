<?php

	App::uses('AppHelper', 'View/Helper');

	class WaterHelper extends AppHelper {

		public $helpers = ['Html'];

		public function textWithIcon($text, $icon) {
			return <<<EOF
				<i class="resp-icon fa fa-$icon"></i>
				<span class="resp-icon-text">$text</span>
EOF;
		}

		public function moreLink($url, $text = null) {
			if ($text === null) {
				$text = '';
			}
			return $this->_iconLink('globe', $url, $text, ['class' => 'moreLink']);
		}

		public function sourceLink($origin) {
			$url = '/s/' . $origin['id'];
			return $this->_iconLink('file-text-o', $url, '', ['class' => 'sourceLink']);
		}

		protected function _iconLink($icon, $url, $text = null, $options = []) {
			if ($text === null) {
				$text = $url;
			}
			$defaults = [
				'class' => 'iconLink',
				'escape' => false,
				'target' => '_blank'
			];
			$options = array_merge_recursive($defaults, $options);
			foreach ($options as $key => $option) {
				if (is_array($option)) {
					$options[$key] = implode(' ', $option);
				}
			}
			$text = $this->textWithIcon($text, $icon);
			return $this->Html->link($text, $url, $options);
		}

	}

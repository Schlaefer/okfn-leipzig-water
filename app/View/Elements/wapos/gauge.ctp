<div class="wapo" data-type=<?= json_encode($gauge['type']) ?>>
	<div class="wapo-content row">
		<div class="wapo-hero col-sm-3">
			<button class="wapo-btn-details isOpen"></button>
			<h2 class="wapo-hero-title"><?= h($gauge['title']) ?></h2>

			<div class="wapo-hero-value"><?= $gauge['value'] ?></div>
			<div class="wapo-hero-unit"><?= h($gauge['unit']) ?></div>
		</div>
		<div class="col-sm-6">
			<div class="gauge"></div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="wapo-details row">
		<div class="col-sm-offset-1 col-sm-8">
			<?php
				echo nl2br(trim(h($gauge['description'])));
				echo '<br><br>';
				echo $this->Water->moreLink($gauge['description-url'],
					$gauge['description-url']);
			?>
		</div>
	</div>
</div>

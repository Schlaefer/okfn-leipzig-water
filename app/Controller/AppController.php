<?php

	App::uses('Controller', 'Controller');
	App::uses('CrudControllerTrait', 'Crud.Lib');

	class AppController extends Controller {

		use CrudControllerTrait;

		public $components = array(
			// 'DebugKit.Toolbar',
			'RequestHandler',
			/*
			'Crud.Crud' => array(
				'actions' => array(
					'index', 'add', 'edit', 'view', 'delete'
				),
				'listeners' => array(
					'api' => 'Crud.Api'
				)
			)
			*/

		);

		public $cacheAction = '+1 hour';

		public $helpers = [
			// 'Cache',
			'Layout',
			'Water'
		];

	}

<?php

	App::uses('AppController', 'Controller');

	class DataOriginsController extends AppController {

		public function view($id = null) {
			if (empty($id)) {
				throw new BadRequestException();
			}

			$origin = $this->DataOrigin->find('first', [
				'contain' => false,
				'conditions' => ['id' => $id]
			]);

			if (empty($origin)) {
				throw new BadRequestException();
			}

			$this->set(compact('origin'));
		}

	}

<?php

	App::uses('AppController', 'Controller');

	class ServiceAreasController extends AppController {

		public $uses = ['Comparison', 'ServiceArea', 'WaMe', 'WaPo'];

		public function index() {
			$zip = $this->request->query('zip');
			if (!$zip) {
				$zip = '04229';
			}
			$zips = $this->ServiceArea->findAllZips();
			if (!in_array($zip, $zips)) {
				throw new BadRequestException;
			}
			$this->set(compact('zips', 'zip'));

			$results = $this->ServiceArea->find('zip', ['zip' => $zip]);

			$this->set('well', $results['Well']);
			$this->set('origin', $results['Well']['WaMe']['DataOrigin']);
			$this->set('gauges', $results['Well']['WaPos']);
			$this->set('comp', $this->Comparison->get());
		}

	}

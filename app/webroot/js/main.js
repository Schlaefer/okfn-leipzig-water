require.config({
  baseUrl: window.app.webroot + 'js/app',
  shim: { bootstrap: { deps: ['jquery'] } },
  paths: {
    jquery: '../../bower_components/jquery/dist/jquery',
    underscore: '../../bower_components/underscore/underscore',
    backbone: '../../bower_components/backbone/backbone',
    marionette: '../../bower_components/marionette/lib/core/amd/backbone.marionette',
    "backbone.babysitter": '../../bower_components/backbone.babysitter/lib/backbone.babysitter',
    "backbone.wreqr": '../../bower_components/backbone.wreqr/lib/backbone.wreqr',
    domReady: '../../bower_components/requirejs-domready/domReady',
    d3: '../../bower_components/d3/d3',
    text: '../../bower_components/requirejs-text/text',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap'
  }
});

require(['app']);

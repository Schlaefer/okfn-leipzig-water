define(['jquery', 'marionette', 'views/compares', 'bootstrap'], function($, Marionette, ComparesView) {

  return Marionette.ItemView.extend({

    ui: {
      'toggleDetails': '#ha'
    },
    events: {
      'click @ui.toggleDetails': '_onToggleDetails'
    },

    initialize: function(options) {
      new ComparesView({ el: '#compare', collection: options.comparisons })
        .render();

      this.bindUIElements();
      this._affix();
    },

    _onToggleDetails: function(event) {
      // @todo
      event.preventDefault();
      $('.wapo-btn-details').click();
    },

    _affix: function() {
      this.$el.addClass('affix-top');

      // @bogus
      if ($('.sticky').css('position').indexOf('sticky') > -1) {
        // this.$el.addClass('sticky');
        return;
      }

      this.$el.affix({
        offset: this.$el.offset().top - 10
      });
    }

  });

});
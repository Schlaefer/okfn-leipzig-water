define(['underscore', 'marionette', 'text!tpls/compare.html', 'text!tpls/sourceLink.html'],
  function(_, Marionette, tpl, sourceLinkTpl) {

    return Marionette.ItemView.extend({
      template: _.template(tpl),
      ui: {
        checkbox: 'input',
        indicator: '.compare-indicator'
      },
      events: {
        'change @ui.checkbox': '_onCheckbox'
      },
      modelEvents: {
        'change:_isShown': '_onIsShown'
      },

      serializeData: function() {
        var data = this.model.toJSON();
        if (data['data_origin_id']) {
          // @todo root
          data.sourceLink = _.template(sourceLinkTpl, {url: window.app.webroot + 's/' + data['data_origin_id']});
        } else {
          data.sourceLink = '';
        }
        return data;
      },

      _onCheckbox: function(event) {
        event.preventDefault();
        this.model.set('_isShown', this.ui.checkbox.prop('checked'));
      },

      _onIsShown: function(model, value) {
        if (value) {
          this.ui.indicator.css({
            color: this.model.get('color'),
            visibility: 'visible'
          });
        } else {
          this.ui.indicator.css({ visibility: 'hidden' });
        }
      }

    });

  });
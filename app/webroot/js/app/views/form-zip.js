define(['jquery', 'underscore', 'marionette'], function($, _, Marionette) {
  'use strict';

  return Marionette.ItemView.extend({
    ui: {
      'btnSend': 'button',
      'textfield': '#zip-textfield'
    },
    events: {
      'click @ui.btnSend': '_submit',
      'keyup @ui.textfield': '_onKeyUp',
      'keydown @ui.textfield': '_onKeyDown'
    },

    _zips: null,

    initialize: function(options) {
      this._zips = options.zips;
      this.bindUIElements();
      this._focus();
    },

    _focus: function() {
      this.ui.textfield.select();
    },

    /**
     * prevents sending form on enter
     *
     * @param event
     * @private
     */
    _onKeyDown: function(event) {
      if (event.keyCode === 13) {
        event.preventDefault();
      }
    },

    _onKeyUp: function(event) {
      event.preventDefault();
      var valid = this._skinForm()
      if (valid && event.keyCode === 13) {
        this._submit();
      }
    },

    _submit: function(event) {
      if(event) { event.preventDefault(); }
      this.$el.submit();
    },

    _skinForm: function() {
      var value = this.ui.textfield.val();

      if (value.length < 5) {
        zip = false
      } else {
        var zip = _.findWhere(this._zips, value);
      }

      if (zip) {
        this.$el.addClass('isValid');
        this.ui.btnSend.show();
      } else {
        this.$el.removeClass('isValid');
        this.ui.btnSend.hide();
      }

      return Boolean(zip);
    }
  });

});
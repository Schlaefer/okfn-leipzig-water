define(['jquery', 'underscore', 'backbone', 'd3'], function($, _, Backbone, d3) {

  'use strict';

  return Backbone.View.extend({
    defaults: {
      height: 60,
      width: 350,
      paddingBottom: 6,
      bar: { height: 35, width: 300  }
    },
    template: _.template(''),
    markers: null,
    _max: null,
    _svg: null,
    _xScale: null,

    initialize: function(options) {
      this.markers = options.markers;

      this.listenTo(this.markers, 'add', this.render);
      this.listenTo(this.markers, 'remove', this.render);
    },

    render: function() {
      this._init();
      this._setScale();
      this._renderSvgContainer();

      this._renderSvgBarBorder();
      this._renderSvgScale();

      this._renderSvgBarValue();
      this._renderSvgBarLimit();

      this._renderSvgMarker();
    },

    _init: function() {
      this._max = this._getMax();
    },

    _getMax: function() {
      var property = this.model.get('type');
      var value = this.model.get('value') * 1.3;
      var limit = this.model.get('limit') * 1.1;
      var maxMarker = this.markers.maxOfProperty(property);
      return Math.round(_.max([maxMarker, value, limit]));
    },

    _setScale: function() {
      var max = this._max;

      this._xScale = d3.scale.linear()
        .domain([0, max])
        .range([0, this._getBarWidth()]);
    },

    _getBarWidth: function() {
      return this.defaults.bar.width;
    },

    _renderSvgBarLimit: function() {
      var bar = this.defaults.bar;
      var limit = this.model.get('limit');

      if (!limit) {
        return;
      }
      limit = this._xScale(limit);

      // select
      var rect = this._getSvg().selectAll('.gauge-limit').data([limit]);

      // create
      rect.enter().append('rect')
        .attr('x', this._getBarWidth())
        .attr('y', this.defaults.height - 1 - bar.height)
        .attr('height', bar.height)
        .attr('width', 0)
        .classed('gauge-limit', true);
      this._svgTransition(rect)
        .attr('x', limit)
        .attr('width', this._getBarWidth() - limit)
    },

    // @todo use d3's scales
    _renderSvgScale: function() {
      var scaleValues = [
        ['0', 0 , 0],
        [this._max, this._getBarWidth(), 0]
      ];

      // @bogus
      this.$('.gauge-scale').remove();

      _.each(scaleValues, function(scaleValue, index) {
        var text = this._renderSvgText(this._getSvg(), scaleValue[0], scaleValue[1], scaleValue[2])
          .attr('opacity', 0)
          .attr('text-anchor', function() {
            if (index === scaleValues.length - 1) {
              return 'end';
            }
            return 'start'
          })
          .classed('gauge-scale', true);
        this._svgTransition(text).attr('opacity', 1);
      }, this);
    },

    _svgTransition: function(object) {
      return object.transition()
        .delay(0)
        .duration(600);
    },

    _renderSvgText: function(object, text, x, y) {
      var fontSize = 15;
      y += fontSize;
      return object.append('text')
        .text(text)
        .attr('x', x)
        .attr('y', y)
    },

    _renderSvgBarValue: function() {
      var bar = this.defaults.bar,
        value = this.model.get('value'),
        scale = this._xScale,
        scaled = scale(value);

      //# filling
      //## setup
      var rect = this._getSvg().selectAll('rect.gauge-value').data([scaled]);

      //## create + update
      rect.enter().append('rect')
        .attr('x', 0)
        .attr('y', this.defaults.height - 1 - bar.height)
        .attr('height', bar.height)
        .attr('width', 0)
        .classed('gauge-value', true);
      this._svgTransition(rect).attr('width', scaled);

      //# scale
      //## setup
      var text = this._getSvg()
        .selectAll('text.gauge-value')
        .data([scaled]);

      // @bogus two updates()
      var update = _.bind(function() {
        var updateText = _.throttle(function(value) {
          var inverted = scale.invert(value);
          text.text(d3.round(inverted), 1)
        }, 100, {leading: false, trailing: false});

        this._svgTransition(text)
          .attr('x', scaled)
          .attrTween('x', function(d, i, a) {
            return function(t) {
              var value = d3.interpolate(a, scaled)(t);
              updateText(value);
              return value;
            };
          })
          .each('end', function() {text.text(value)});
      }, this);

      update();

      //## create
      text = this._renderSvgText(text.enter(), value, 0, 0)
        .attr('text-anchor', 'middle')
        .classed('gauge-value', true);

      update();

    },

    _renderSvgBarBorder: function() {
      var bar = this.defaults.bar;
      var border = this._getSvg().selectAll('.gauge-border').data(['dummy']);

      border.enter().append('rect')
        .attr('x', 0)
        .attr('y', this.defaults.height - 1 - bar.height)
        .attr('height', bar.height)
        .attr('width', this._getBarWidth())
        .attr('stroke-width', 1)
        .classed('gauge-border', true);
    },

    _getSvg: function() {
      return this._svg;
    },

    _renderSvgContainer: function() {
      if (this._svg) {
        return;
      }
      this._svg = d3.select(this.el).
        append('svg').
        attr('width', this.defaults.width).
        attr('height', this.defaults.height + this.defaults.paddingBottom);
    },

    _renderSvgMarker: function(data) {
      var margin = 4,
        scale = this._xScale,
        bar = this.defaults.bar,
        data = this._getMarkerData() || [];

      var marker = this._getSvg().selectAll('.gauge-marker')

      marker = marker.data(data, function(d) { return d.value; });

      // create
      marker.enter().append('rect')
        .attr('x', 0)
        .attr('y', this.defaults.height - 1 - bar.height - margin)
        .attr('height', bar.height + (margin * 2))
        .attr('width', 5)
        .attr('fill', function(d) { return d.color; })
        .classed('gauge-marker', true);

      // create + update
      this._svgTransition(marker).attr('x', function(d) { return scale(d.value); });

      // remove
      marker.exit().transition().attr('x', 0).remove();
    },

    _getMarkerData: function() {
      var data = [];
      var type = this.model.get('type');

      this.markers.each(function(model) {
        var value = model.get(type);
        var color = model.get('color');
        if (value) {
          data.push({ value: value, color: color });
        }
      });

      return data;
    }

  });

});

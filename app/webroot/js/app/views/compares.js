define(['marionette', 'views/compare'], function(Marionette, CompareView) {

  return Marionette.CollectionView.extend({
    itemView: CompareView
  });

});
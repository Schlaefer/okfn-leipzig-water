define(['marionette', 'views/gauge'], function(Marionette, GaugeView) {
  'use strict';

  return Marionette.ItemView.extend({
    ui: {
      btnDetails: '.wapo-btn-details',
      details: '.wapo-details'
    },
    events: {
      'click @ui.btnDetails': '_onShowDetails'
    },

    initialize: function(options) {
      var gauge =  new GaugeView({
        el: this.$('.gauge'),
        model: this.model,
        markers: options.markers
      });
      gauge.render();

      this.bindUIElements();
    },

    _onShowDetails: function() {
      this.ui.btnDetails.toggleClass('isOpen');
      this.ui.details.slideToggle();
    }

  });

});
define(['backbone', 'models/gauge'], function(Backbone, GaugesModel) {
  'use strict';

  return Backbone.Collection.extend({
    model: GaugesModel
  });

});

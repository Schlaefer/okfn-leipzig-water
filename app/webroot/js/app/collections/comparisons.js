define(['backbone'], function(Backbone) {
  'use strict';

  return Backbone.Collection.extend({

    defaults: {
      isShown: false
    }

  });

});
define(['underscore', 'backbone'], function(_, Backbone) {
  'use strict';

  return Backbone.Collection.extend({
    comparisons: null,
    model: Backbone.Model.extend(),

    initialize: function(models, options) {
      this.comparisons = options.comparisons;

      this.listenTo(this.comparisons, 'change:_isShown', this._onIsShown);
    },

    _onIsShown: function(model, isShown) {
      if (isShown) {
        this.add(model);
      } else {
        this.remove(model);
      }
    },

    maxOfProperty: function(property) {
      var max = this.max(function(marker) {
        return marker.get(property);
      });
      return _.isObject(max) ? max.get(property) : null;
    }

  });

});
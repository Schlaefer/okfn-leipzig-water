define(['jquery', 'collections/comparisons', 'collections/gauges', 'collections/markers', 'views/wapo', 'domReady', 'views/sidebar', 'views/form-zip'],
  function($, ComparisonsCollection, GaugesCollection, MarkersCollection, WapoView, domReady, SidebarView, FormZipView) {

    var AppInit = window.app;
    if (!AppInit.data) {
      return;
    }
    var gauges = new GaugesCollection(AppInit.data.wapos);
    var comparisons = new ComparisonsCollection(AppInit.data.comp);
    var markers = new MarkersCollection(null, {comparisons: comparisons});

    var initFromDom = function() {

      var wapos = $('.wapo');
      _.each(wapos, function(wapo) {
        var $wapo = $(wapo);
        var type = $wapo.data('type');
        var model = gauges.findWhere({ type: type });
        new WapoView({ el: $wapo, model: model, markers: markers });
      });

      new SidebarView({ el: '#sidebar', comparisons: comparisons });
      new FormZipView({ el: '#form-zip', zips: AppInit.data.zips });
    }

    domReady(initFromDom);
  });
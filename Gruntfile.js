module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      production: {
        files: [
          {
            expand: true,
            cwd: './app/webroot/bower_components/fontawesome/fonts/',
            src: '*',
            dest: './app/webroot/dist/fonts'
          },
          {
            expand: true,
            cwd: './app/webroot/bower_components/requirejs/',
            src: 'require.js',
            dest: './app/webroot/dist'
          }
        ]
      }
    },
    less: {
      production: {
        options: {
          modifyVars: {
            assets: '"../css/assets"',
            "fa-font-path": '"fonts"'
          }
        },
        files: {
          "./app/webroot/css/app.css": "./app/webroot/css/styles.less",
          "./app/webroot/css/fontawesome.css": "./app/webroot/bower_components/fontawesome/less/font-awesome.less"
        }
      }
    },
    cssmin: {
      combine: {
        files: {
          './app/webroot/dist/styles.min.css': [
            './app/webroot/bower_components/bootstrap/dist/css/bootstrap.css',
            './app/webroot/css/fontawesome.css',
            './app/webroot/css/app.css'
          ]
        }
      }
    },
    requirejs: {
      compile: {
        options: {
          baseUrl: "./app/webroot/js/app",
          shim: { bootstrap: { deps: ['jquery'] } },
          optimize: "uglify2",
          paths: {
            jquery: '../../bower_components/jquery/dist/jquery',
            underscore: '../../bower_components/underscore/underscore',
            backbone: '../../bower_components/backbone/backbone',
            marionette: '../../bower_components/marionette/lib/core/amd/backbone.marionette',
            "backbone.babysitter": '../../bower_components/backbone.babysitter/lib/backbone.babysitter',
            "backbone.wreqr": '../../bower_components/backbone.wreqr/lib/backbone.wreqr',
            domReady: '../../bower_components/requirejs-domready/domReady',
            d3: '../../bower_components/d3/d3',
            text: '../../bower_components/requirejs-text/text',
            bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap'
          },
          name: "./../main",
          out: "./app/webroot/dist/main.min.js"
        }
      }
    },
    clean: {
      css: ["./app/webroot/css/app.css", "./app/webroot/css/fontawesome.css"]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('css', ['less:production', 'cssmin', 'clean:css']);
  grunt.registerTask('default', ['requirejs', 'css', 'copy:production']);
};



# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.33)
# Database: wasser
# Generation Time: 2014-05-19 15:54:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table comparisons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comparisons`;

CREATE TABLE `comparisons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_origin_id` int(11) unsigned DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `sodium` float DEFAULT NULL COMMENT 'Natrium',
  `potassium` float DEFAULT NULL COMMENT 'Kalium',
  `calcium` float DEFAULT NULL,
  `magnesium` float DEFAULT NULL,
  `chloride` float DEFAULT NULL,
  `nitrate` float DEFAULT NULL,
  `sulfate` float DEFAULT NULL,
  `hardness` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comparisons` WRITE;
/*!40000 ALTER TABLE `comparisons` DISABLE KEYS */;

INSERT INTO `comparisons` (`id`, `data_origin_id`, `label`, `sodium`, `potassium`, `calcium`, `magnesium`, `chloride`, `nitrate`, `sulfate`, `hardness`, `created`, `modified`)
VALUES
	(1,2,'Apolinaris',480,30,90,120,130,NULL,100,NULL,NULL,NULL),
	(2,3,'Evian',6.5,1,80,26,8.6,3.7,12.6,7.2,NULL,NULL),
	(3,4,'Gaensefurther',59,10.9,193,88.5,182,0.5,475,NULL,NULL,NULL);

/*!40000 ALTER TABLE `comparisons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table data_origins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `data_origins`;

CREATE TABLE `data_origins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text,
  `url` varchar(255) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `data_origins` WRITE;
/*!40000 ALTER TABLE `data_origins` DISABLE KEYS */;

INSERT INTO `data_origins` (`id`, `description`, `url`, `created`, `modified`)
VALUES
	(1,'Jahresdurchschnittsanalyse 2013 der Trinkwasserabgabestellen der Kommunalen Wasserwerke Leipzig GmbH','http://www.wasser-leipzig.de/get.php?f=5fa9ba4a3ebc6fcadb34c9b9c3a1be0b.pdf&m=download',NULL,NULL),
	(2,'Wikipedia','http://de.wikipedia.org/wiki/Apollinaris_(Mineralwasser)',NULL,NULL),
	(3,'Wikipedia','http://en.wikipedia.org/wiki/Evian',NULL,NULL),
	(4,'Geaesefurther Homepage','http://www.gaensefurther.de/produkte/mineralwasser/classic.html',NULL,NULL);

/*!40000 ALTER TABLE `data_origins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table service_areas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `service_areas`;

CREATE TABLE `service_areas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `well_id` int(11) DEFAULT NULL,
  `ZIP` varchar(9) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ZIP` (`ZIP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `service_areas` WRITE;
/*!40000 ALTER TABLE `service_areas` DISABLE KEYS */;

INSERT INTO `service_areas` (`id`, `well_id`, `ZIP`, `created`, `modified`)
VALUES
	(1,1,'04229',NULL,NULL),
	(2,1,'04318',NULL,NULL),
	(3,1,'04289',NULL,NULL),
	(4,1,'04420',NULL,NULL),
	(5,1,'04249',NULL,NULL),
	(6,1,'04460',NULL,NULL),
	(7,1,'04207',NULL,NULL),
	(8,1,'04209',NULL,NULL),
	(9,1,'04205',NULL,NULL),
	(10,1,'04178',NULL,NULL),
	(11,1,'04177',NULL,NULL),
	(12,1,'04277',NULL,NULL),
	(13,1,'04279',NULL,NULL),
	(14,1,'04275',NULL,NULL),
	(15,1,'04107',NULL,NULL),
	(16,1,'04109',NULL,NULL),
	(17,1,'04179',NULL,NULL),
	(18,1,'04105',NULL,NULL),
	(19,1,'04155',NULL,NULL),
	(20,1,'04157',NULL,NULL),
	(22,1,'04299',NULL,NULL),
	(23,1,'04103',NULL,NULL),
	(24,1,'04317',NULL,NULL),
	(26,1,'04315',NULL,NULL);

/*!40000 ALTER TABLE `service_areas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wa_mes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wa_mes`;

CREATE TABLE `wa_mes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `well_id` int(11) unsigned NOT NULL,
  `data_origin_id` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wa_mes` WRITE;
/*!40000 ALTER TABLE `wa_mes` DISABLE KEYS */;

INSERT INTO `wa_mes` (`id`, `well_id`, `data_origin_id`, `date`, `created`, `modified`)
VALUES
	(1,1,1,'2013-12-01 01:01:01',NULL,NULL);

/*!40000 ALTER TABLE `wa_mes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wa_po_defs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wa_po_defs`;

CREATE TABLE `wa_po_defs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `unit` varchar(16) NOT NULL DEFAULT '',
  `limit` float DEFAULT NULL,
  `recommended` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wa_po_defs` WRITE;
/*!40000 ALTER TABLE `wa_po_defs` DISABLE KEYS */;

INSERT INTO `wa_po_defs` (`id`, `title`, `unit`, `limit`, `recommended`, `created`, `modified`)
VALUES
	(1,'sodium','mg/l',200,NULL,NULL,NULL),
	(2,'potassium','mg/l',NULL,NULL,NULL,NULL),
	(3,'calcium','mg/l',NULL,NULL,NULL,NULL),
	(4,'magnesium','mg/l',NULL,NULL,NULL,NULL),
	(5,'chloride','mg/l',250,NULL,NULL,NULL),
	(6,'nitrate','mg/l',50,NULL,NULL,NULL),
	(7,'sulfate','mg/l',250,NULL,NULL,NULL),
	(8,'hardness','°dH',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `wa_po_defs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wa_pos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wa_pos`;

CREATE TABLE `wa_pos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wa_me_id` int(11) NOT NULL,
  `wa_po_def_id` int(11) NOT NULL,
  `value` float DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wa_pos` WRITE;
/*!40000 ALTER TABLE `wa_pos` DISABLE KEYS */;

INSERT INTO `wa_pos` (`id`, `wa_me_id`, `wa_po_def_id`, `value`, `created`, `modified`)
VALUES
	(1,1,1,25.2,NULL,NULL),
	(2,1,2,5.4,NULL,NULL),
	(3,1,3,79.8,NULL,NULL),
	(4,1,4,15.2,NULL,NULL),
	(5,1,5,43.7,NULL,NULL),
	(6,1,6,19,NULL,NULL),
	(7,1,7,165,NULL,NULL),
	(8,1,8,14.7,NULL,NULL);

/*!40000 ALTER TABLE `wa_pos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wells
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wells`;

CREATE TABLE `wells` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `wells` WRITE;
/*!40000 ALTER TABLE `wells` DISABLE KEYS */;

INSERT INTO `wells` (`id`, `title`, `created`, `modified`)
VALUES
	(1,'WVA Probstheida',NULL,NULL);

/*!40000 ALTER TABLE `wells` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
